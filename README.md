# Path Go - React Web App

## Meta

[![Netlify Status](https://api.netlify.com/api/v1/badges/3bfc2db8-22a5-43d0-94db-7c04187cca8d/deploy-status)](https://app.netlify.com/sites/vibrant-feynman-9443ac/deploys)

[PathGo Live Preview](http://pathgo.app/)

## Team members

- Asif Shikder
- Calvin Costa
- Benny Chan
- Alvin Rosario

## Quick Start

1. Install Dependencies: `yarn install`
2. Create a `.env` file at the root of the project and the following entries
   ```
   REACT_APP_USER_POOL_ID=<string>
   REACT_APP_USER_POOL_CLIENT_ID=<string>
   REACT_APP_USER_POOL_REGION=<string>
   REACT_APP_GEOCODE_API_KEY=<string>
   ```
   You will need the appropriate Cognito credential for those
3. Run the App: `yarn start`
4. ???
5. Profit

## Scripts

- `yarn run test`: Run all tests
