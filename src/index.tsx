import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { BrowserRouter as Router } from "react-router-dom";
import Amplify from "aws-amplify";
import { AuthConfig } from "./config/amplify";
import { UserContextProvider } from "./context/user";
import App from "./app";

Amplify.configure({
  Auth: AuthConfig,
});

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <UserContextProvider>
        <App />
      </UserContextProvider>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);
