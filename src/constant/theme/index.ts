export enum ThemeColor {
  Primary = "#3D5A80",
  Secondary = "#EE6C4D",
  Info = "#98C1D9",
  Dark = "#293241",
  Light = "#E0FBFC",
  Black = "#000000",
  Danger = "#d62828",
  Success = "#2a9d8f",
  Warning = "#FFDD4A",
  White = "#FFFFFF",
}

export const Theme = {
  color: ThemeColor,
};
