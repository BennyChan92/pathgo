import React from "react";
import { BrowserRouter } from "react-router-dom";

const TestPageWrapper: React.FC = ({ children }) => {
  return <BrowserRouter>{children}</BrowserRouter>;
};

export default TestPageWrapper;
