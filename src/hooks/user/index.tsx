import { useContext } from "react";
import UserContext from "../../context/user";

export const useIsAuthenticated = (): boolean => {
  const { user } = useContext(UserContext);
  return user !== undefined;
};

export const useUserId = (): string | undefined => {
  const { user } = useContext(UserContext);
  return user?.id;
};
