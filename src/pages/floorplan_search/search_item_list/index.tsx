import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { FormControlLabel, Switch } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { BusinessModel } from "../../../service/api/types";
import FloorPlanSearchListItem from "./search_list_item";

const useStyles = makeStyles({
  optionWrapper: {
    display: "flex",
    justifyContent: "flex-end",
  },
  root: {
    marginTop: "24px",
    flexGrow: 1,
    marginLeft: "50px",
    marginRight: "50px",
  },
});

interface ResultProps {
  results: Array<BusinessModel>;
}

const FloorPlanSearchItemList: React.FC<ResultProps> = ({ results }) => {
  const classes = useStyles();
  const history = useHistory();
  const [navToEdit, setNavToEdit] = useState(false);

  const handleSelectFloor = (id: string) => {
    if (navToEdit) {
      history.push(`/floorplan/${id}/edit`);
    } else {
      history.push(`/pathfinder/${id}/prompt`);
    }
  };

  return (
    <div className={classes.root}>
      {results.length !== 0 && (
        <div className={classes.optionWrapper}>
          <FormControlLabel
            control={
              <Switch
                checked={navToEdit}
                onChange={(e) => setNavToEdit(e.target.checked)}
              />
            }
            label="Dev - Nav to Edit"
          />
        </div>
      )}
      <div>
        {results.map((item, index) => (
          <FloorPlanSearchListItem
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            businessName={item.businessName}
            address={item.address.streetAddress}
            onClick={() => handleSelectFloor(item.id)}
          />
        ))}
      </div>
    </div>
  );
};

export default FloorPlanSearchItemList;
