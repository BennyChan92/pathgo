import React from "react";
import QRCode from "qrcode.react";
import { useErrorAlert } from "../../hooks/alert";
import useQueryParams from "../../utils/router/use_query_params";

const QRCodeGeneratorPage: React.FC = () => {
  const { businessId } = useQueryParams<{ businessId: string }>();
  const showError = useErrorAlert();
  if (businessId === undefined) {
    showError("Business ID is not defined");
  }
  const qrpath = `https://pathgo.app/pathfinder/${businessId}/prompt`;

  return <QRCode size={512} value={qrpath} />;
};

export default QRCodeGeneratorPage;
