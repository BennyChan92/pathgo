import React, { useEffect, useState } from "react";
import TextField from "@material-ui/core/TextField";
import { Container, makeStyles, MenuItem } from "@material-ui/core";
import axios from "axios";
import { ThemeColor } from "../../constant/theme";

const useBranchPreviewStyles = makeStyles({
  previewUrlWrapper: {
    marginTop: "20px",
    backgroundColor: ThemeColor.Light,
  },
  urlDisplay: {
    padding: "20px",
  },
});

const BranchPreviewUrl = () => {
  const classes = useBranchPreviewStyles();
  const [branchName, setBranchName] = useState("");
  const [branchList, setBranchList] = useState([]);

  useEffect(() => {
    const getBranchList = async () => {
      const response = await axios.get(
        "https://api.bitbucket.org/2.0/repositories/pathgo/reinhardt/refs/branches"
      );
      const branches = response.data.values
        .sort(
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (entry1: any, entry2: any) =>
            new Date(entry1.target.date).getTime() >
            new Date(entry2.target.date).getTime()
        ) // Sort by newly created branches
        .map((entry: { name: string }) => entry.name);

      setBranchList(branches);
      setBranchName(branches[0] || "");
    };

    getBranchList();
    // eslint-disable-next-line
  }, []);

  const handleChange = (newValue: string) => {
    setBranchName(newValue);
  };

  const getBranchUrl = (branch: string) =>
    `https://${branch}--vibrant-feynman-9443ac.netlify.app/`;

  return (
    <div>
      <TextField
        label="Branch Title"
        variant="outlined"
        value={branchName}
        select
        onChange={(e) => handleChange(e.target.value)}
      >
        {branchList.map((entry) => (
          <MenuItem key={entry} value={entry}>
            {entry}
          </MenuItem>
        ))}
      </TextField>
      {branchName.length > 0 && (
        <div className={classes.previewUrlWrapper}>
          <Container className={classes.urlDisplay}>
            {/* eslint-disable-next-line react/jsx-no-target-blank */}
            <a target="_blank" href={getBranchUrl(branchName)}>
              {getBranchUrl(branchName)}
            </a>
          </Container>
        </div>
      )}
    </div>
  );
};

const Tools: React.FC = () => {
  return (
    <div>
      <h2>Tools</h2>
      <h3>Branch Preview</h3>
      <BranchPreviewUrl />
    </div>
  );
};

export default Tools;
