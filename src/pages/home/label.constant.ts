export default {
  ButtonLabel: {
    Login: "Login Page",
    SignUp: "Sign Up Page",
    CreateFloorPlan: "Create Floor Plan Page",
    SearchFloorPlan: "Search Floor Plan Page",
    EditFloorPlan: "Edit Floor Plan Page",
    PromptPathFinder: "Path Finder Prompt Page",
    PathFinderNav: "Path Finder Navigation Page",
    Error404: "404 Page",
    BusinessDashboard: "Business Dashboard",
  },
};
