import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";
import useFormChange from "../../../components/form/textfield/useFormChange";
import { ThemeColor } from "../../../constant/theme";
import TextField, {
  TextFieldVariant,
} from "../../../components/form/textfield";

export type HeroSearchProps = {
  onSearch: (title: string) => void;
  initialTitle?: string;
};

const useStyles = makeStyles({
  root: {},
  search: {
    backgroundColor: ThemeColor.Light,
    // border: "none",
    // outline: "none",
  },
});

const HeroSearch: React.FC<HeroSearchProps> = ({
  onSearch,
  initialTitle = "",
}) => {
  const classes = useStyles();
  const [businessName, setbusinessName] = useFormChange(initialTitle);
  useEffect(() => {
    const handleEnterKey = (e: KeyboardEvent) => {
      if (e.key === "Enter") {
        onSearch(businessName);
      }
    };
    document.addEventListener("keydown", handleEnterKey);

    return () => {
      document.removeEventListener("keydown", handleEnterKey);
    };
  }, [businessName, onSearch]);
  return (
    <div className={classes.root}>
      <Container>
        <TextField
          fullWidth
          label="Business Name"
          className={classes.search}
          type="search"
          variant={TextFieldVariant.Filled}
          value={businessName}
          onChange={setbusinessName}
        />
      </Container>
    </div>
  );
};
export default HeroSearch;
