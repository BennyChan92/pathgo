import { Grid, makeStyles } from "@material-ui/core";
import React from "react";
import { ThemeColor } from "../../../constant/theme";
import RecentBusinessComponent from "./recent";
import PopularBusinessComponent from "./popular";
import NearMeBusinessComponent from "./near_me";

const useStyles = makeStyles({
  headerText: {
    margin: 0,
    textAlign: "center",
    fontSize: "2rem",
  },

  textcolor: {
    color: ThemeColor.Secondary,
  },
  card: {
    margin: "20px",
  },
  root: {
    flexGrow: 1,
    maxWidth: "100vw",
    height: "100%",
    minHeight: "40vh",
    margin: "0px",
    backgroundColor: "#d3d3d3",
    paddingTop: "10px",
  },
  paper: {
    padding: "18px",
    textAlign: "center",
    color: ThemeColor.Secondary,
    minHeight: "20vh",
  },
});

const RecommendedPlaces: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div>
        <h3 className={classes.headerText}>
          <span className={classes.textcolor}>Explore</span> PathGo
        </h3>
      </div>
      <div className={classes.card}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={4}>
            <RecentBusinessComponent />
          </Grid>
          <Grid item xs={12} sm={4}>
            <NearMeBusinessComponent />
          </Grid>
          <Grid item xs={12} sm={4}>
            <PopularBusinessComponent />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};
export default RecommendedPlaces;
