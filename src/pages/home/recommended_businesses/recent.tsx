import React, { useEffect, useState } from "react";
import AccessTimeSharpIcon from "@material-ui/icons/AccessTimeSharp";
import BusinessService from "../../../service/api/business";
import { BusinessModel } from "../../../service/api/types";
import RecommendedCategory from "./recommended_category";
import AppStorage from "../../../service/local_storage";
import { useErrorAlert } from "../../../hooks/alert";

const HandleFetchRecentBusiness = async (
  businessId: string
): Promise<BusinessModel | null> => {
  try {
    const business = await BusinessService.FindById(businessId);
    return business;
  } catch (error) {
    AppStorage.RemoveFromRecentIfExists(businessId);
    return null;
  }
};

const RecentBusinessComponent: React.FC = () => {
  const [recentlySearchResults, setrecentlySearchResults] = useState<
    Array<BusinessModel>
  >([]);
  const showError = useErrorAlert();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const handleFetchPopular = async () => {
      try {
        setLoading(true);
        const businesses = await Promise.all(
          AppStorage.RecentlyVisited().map((businessId) =>
            HandleFetchRecentBusiness(businessId)
          )
        );
        const validBusinesses = businesses.filter(
          (business) => business !== null
        );
        setrecentlySearchResults(validBusinesses as BusinessModel[]);
      } catch (error) {
        showError(error.message || "Unable to fetch popular businesses");
      } finally {
        setLoading(false);
      }
    };

    handleFetchPopular();
  }, []);

  return (
    <RecommendedCategory
      loading={loading}
      title="Recently Visited"
      businesses={recentlySearchResults}
      icon={AccessTimeSharpIcon}
    />
  );
};

export default RecentBusinessComponent;
