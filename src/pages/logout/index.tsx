import React, { useContext, useEffect, useMemo, useState } from "react";
import LinearProgress from "../../components/linear_progress";
import NavBar from "../../components/navbar";
import Text from "../../components/Text";
import { StandardReguarUserNavItems } from "../../config/navbar";
import { ThemeColor } from "../../constant/theme";
import UserContext from "../../context/user";
import AuthService from "../../service/auth";

const LogoutPage: React.FC = () => {
  const { clearUser } = useContext(UserContext);
  const [errorMessage, setErrorMessage] = useState();
  const [loading, setLoading] = useState(false);
  const [loginSuccessful, setLoginSuccessful] = useState(false);

  useEffect(() => {
    const handleLogout = async () => {
      setLoading(true);
      try {
        await AuthService.Logout();
        clearUser();
        setLoginSuccessful(true);
      } catch (error) {
        setErrorMessage(error.message || "Unable to logout");
      } finally {
        setLoading(false);
      }
    };

    handleLogout();
  }, [clearUser]);

  const logoutFeedbackText = useMemo(() => {
    if (loginSuccessful) {
      return <Text subtitle>Successfully logged out!</Text>;
    }
    if (errorMessage) {
      return (
        <Text subtitle color={ThemeColor.Danger}>
          {errorMessage}
        </Text>
      );
    }
    return <Text subtitle>Please wait while we log you out</Text>;
  }, [errorMessage, loginSuccessful]);

  return (
    <div>
      <NavBar navItems={StandardReguarUserNavItems} />
      {loading && <LinearProgress />}
      <h1>Logout</h1>
      {logoutFeedbackText}
    </div>
  );
};

export default LogoutPage;
