import React, { useContext, useState } from "react";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import useFormChange from "../../components/form/textfield/useFormChange";
import TextFieldComponent from "../../components/form/textfield";
import AuthService from "../../service/auth";
import UserContext from "../../context/user";
import AuthLayout from "../../components/layout/auth";

const useStyles = makeStyles({
  textField: {
    marginBottom: "12px",
  },
});
const LoginPage: React.FC = () => {
  const classes = useStyles();
  // ========= Form Inputs =========
  const [email, setemail] = useFormChange("");
  const [password, setpassword] = useFormChange("");

  const [errorMessage, setErrorMessage] = useState<string>();
  const history = useHistory();
  const { setUser } = useContext(UserContext);

  const handleLogin = async () => {
    if (errorMessage) setErrorMessage(undefined);
    const user = await AuthService.Login(email, password);
    setUser(user);
    history.push(`/`);
  };

  return (
    <AuthLayout title="Welcome Back" buttonTitle="Login" onSubmit={handleLogin}>
      <TextFieldComponent
        className={classes.textField}
        required
        fullWidth
        autoComplete="email"
        type="email"
        label="Email"
        placeholder="Email"
        value={email}
        onChange={setemail}
      />
      <TextFieldComponent
        required
        fullWidth
        type="password"
        label="Password"
        autoComplete="password"
        placeholder="Password"
        value={password}
        onChange={setpassword}
      />
    </AuthLayout>
  );
};
export default LoginPage;
