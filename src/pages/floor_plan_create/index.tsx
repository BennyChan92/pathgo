import React, { useState } from "react";
import { makeStyles, MenuItem } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import TextFieldComponent from "../../components/form/textfield";
import Button from "../../components/button";
import useFormChange from "../../components/form/textfield/useFormChange";
import BusinessService from "../../service/api/business";
import Text from "../../components/Text";
import { ThemeColor } from "../../constant/theme";
import NavBar from "../../components/navbar";
import { StandardAuthUserNavItems } from "../../config/navbar";
import stateList from "./state_list.json";

const useHomepageStyles = makeStyles({
  wrapper: {
    marginTop: "72px",
    marginLeft: "auto",
    marginRight: "auto",
    maxWidth: "60vw",
  },
  header: {
    margin: 0,
    textAlign: "center",
  },
  formStyle: {
    paddingTop: "24px",
    paddingBottom: "24px",
    border: "3px solid #3D5A80",
    padding: "0 24px",
  },
  button: {
    marginTop: "24px",
    textAlign: "center",
  },
  error: {
    margin: "12px 0",
  },
  textField: {
    margin: "12px 0px",
  },
  stateInput: {
    width: "200px",
  },
});

const FloorPlanCreatePage: React.FC = () => {
  const classes = useHomepageStyles();
  // ========= Form Inputs =========
  const [businessName, setbusinessName] = useFormChange("");
  const [address, setaddress] = useFormChange("");
  const [state, setstate] = useFormChange("NY");
  const [city, setcity] = useFormChange("");
  const [zip, setzip] = useFormChange("");

  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string>();
  const history = useHistory();

  const handleCreateBusiness = async () => {
    try {
      if (errorMessage) setErrorMessage(undefined);
      setLoading(true);
      if (zip.length !== 5) throw new Error("Zipcode must have a length of 5");
      const data = {
        businessName,
        address: {
          streetAddress: address,
          state,
          city,
          zipcode: parseInt(zip, 10),
        },
      };
      const response = await BusinessService.Create(data);
      history.push(`/floorplan/${response.id}/edit`);
    } catch (error) {
      setErrorMessage(error.message || "Something went wrong");
      setLoading(false);
    }
  };

  return (
    <div>
      <NavBar navItems={StandardAuthUserNavItems} />
      <div className={classes.wrapper}>
        <h1 className={classes.header}>Submit your Business</h1>
        <form className={classes.formStyle}>
          {errorMessage && (
            <Text className={classes.error} color={ThemeColor.Danger}>
              {errorMessage}
            </Text>
          )}
          <TextFieldComponent
            className={classes.textField}
            required
            fullWidth
            autoComplete="organization"
            type="name"
            label="Business Name"
            placeholder="Business Name"
            value={businessName}
            onChange={setbusinessName}
          />
          <TextFieldComponent
            required
            className={classes.textField}
            fullWidth
            autoComplete="street-address"
            placeholder="Street Address"
            value={address}
            onChange={setaddress}
          />
          <div>
            <TextFieldComponent
              required
              label="State"
              select
              className={`${classes.stateInput} ${classes.textField}`}
              value={state}
              onChange={setstate}
            >
              {stateList.map((current) => (
                <MenuItem
                  key={current.abbreviation}
                  value={current.abbreviation}
                >
                  {current.name}
                </MenuItem>
              ))}
            </TextFieldComponent>
          </div>
          <TextFieldComponent
            className={classes.textField}
            required
            placeholder="City"
            value={city}
            onChange={setcity}
          />
          <TextFieldComponent
            required
            className={classes.textField}
            autoComplete="postal-code"
            placeholder="Zip Code"
            type="number"
            value={zip}
            onChange={setzip}
          />
          <div className={classes.button}>
            <Button disabled={loading} onClick={handleCreateBusiness}>
              Submit
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
};
export default FloorPlanCreatePage;
