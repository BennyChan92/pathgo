import { makeStyles } from "@material-ui/core";
import React from "react";
import { ThemeColor } from "../../../constant/theme";

const useStyles = makeStyles({
  wrapper: {
    background: ThemeColor.Secondary,
    flex: "0 0 5%",
    minHeight: "100%" /* chrome needed it a question time , not anymore */,
  },
});

const FloorSelector: React.FC = () => {
  const classes = useStyles();
  return <div className={classes.wrapper}>Floor Selector</div>;
};

export default FloorSelector;
