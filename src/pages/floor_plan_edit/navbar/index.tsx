import { makeStyles } from "@material-ui/core";
import React, { useMemo } from "react";
import { useHistory } from "react-router-dom";
import Button from "../../../components/button";
import NavBar from "../../../components/navbar";
import { ThemeColor } from "../../../constant/theme";

type FloorPlanNavBarProps = {
  title: string;
  onSave: () => Promise<void>;
};

const useStyles = makeStyles({
  homeButton: {
    marginRight: "20px",
  },
});

// TODO: Remove the eslint disable line from below
const FloorPlanNavBar: React.FC<FloorPlanNavBarProps> = ({ title, onSave }) => {
  // TODO: Set Title to the given prop from FloorPlanEditPage component
  // TODO: Render Save Button. In click save, call onSave function
  const history = useHistory();
  const classes = useStyles();
  const rightContent = useMemo(
    () => (
      <>
        <Button
          className={classes.homeButton}
          color={ThemeColor.Light}
          onClick={() => history.push("/")}
        >
          Home
        </Button>
        <Button color={ThemeColor.Primary} onClick={onSave}>
          Save
        </Button>
      </>
    ),
    [classes.homeButton, history, onSave]
  );
  return <NavBar title={title} right={rightContent} />;
};

export default FloorPlanNavBar;
