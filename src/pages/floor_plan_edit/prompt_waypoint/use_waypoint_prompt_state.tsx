import { useState } from "react";
import { UseGridWaypointActionHookOutput } from "../grid_action_hooks/type";

export type WaypointPromptModalStateHook = (
  props: UseGridWaypointActionHookOutput
) => {
  displayPrompt: boolean;
  waypointPromptCurrentTitle?: string;
  openWaypointPrompt: (
    waypointNodeRow: number,
    waypointNodeCol: number,
    title?: string
  ) => void;
  onCancelCreateWaypoint: () => void;
  onSaveWaypoint: (title: string) => void;
  onDeleteWaypointCurrentWaypoint: () => void;
};

const useWaypointPromptModalState: WaypointPromptModalStateHook = ({
  setWaypoint,
  removeWaypoint,
}) => {
  const [displayPrompt, setDisplayPrompt] = useState(false);
  const [targetNodeIndex, setTargetNodeIndex] = useState<[number, number]>();
  const [waypointPromptCurrentTitle, setCurrentTitle] = useState<string>();

  const openWaypointPrompt = (
    waypointNodeRow: number,
    waypointNodeCol: number,
    title?: string
  ) => {
    setTargetNodeIndex([waypointNodeRow, waypointNodeCol]);
    setDisplayPrompt(true);
    setCurrentTitle(title);
  };

  const onCancelCreateWaypoint = () => {
    setDisplayPrompt(false);
  };

  const onSaveWaypoint = (title: string) => {
    if (targetNodeIndex === undefined)
      throw new Error("No target waypoint node found");
    const [row, col] = targetNodeIndex;
    setWaypoint(row, col, title);
    setCurrentTitle(undefined);
    setDisplayPrompt(false);
  };

  const onDeleteWaypointCurrentWaypoint = () => {
    if (targetNodeIndex === undefined)
      throw new Error("No target waypoint node found");
    const [row, col] = targetNodeIndex;
    removeWaypoint(row, col);
    setDisplayPrompt(false);
  };

  return {
    displayPrompt,
    waypointPromptCurrentTitle,
    openWaypointPrompt,
    onSaveWaypoint,
    onCancelCreateWaypoint,
    onDeleteWaypointCurrentWaypoint,
  };
};

export default useWaypointPromptModalState;
