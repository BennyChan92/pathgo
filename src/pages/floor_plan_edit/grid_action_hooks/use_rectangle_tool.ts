import { useState } from "react";
import { FloorGridNodeType } from "../../../service/api/types";
import { UseGridActionHook } from "./type";

const useRectangleTool: UseGridActionHook = (gridNodes, setGridNodes) => {
  const [rectangleStartIndex, setRectangleStartIndex] = useState<
    undefined | [number, number]
  >();

  const nodeSelected = (rowSelected: number, colSelected: number): void => {
    if (rectangleStartIndex === undefined) {
      const newGridNodes = [...gridNodes];
      newGridNodes[rowSelected][colSelected].highlight = true;
      setGridNodes(newGridNodes);
      setRectangleStartIndex([rowSelected, colSelected]);
    } else {
      const [
        initialRowSelectedIndex,
        initialColSelectedIndex,
      ] = rectangleStartIndex;
      const [startRow, endRow] = [
        Math.min(rowSelected, initialRowSelectedIndex),
        Math.max(rowSelected, initialRowSelectedIndex),
      ];
      const [startCol, endCol] = [
        Math.min(colSelected, initialColSelectedIndex),
        Math.max(colSelected, initialColSelectedIndex),
      ];
      const newGridRow = [...gridNodes];
      // Wall off cols
      for (let col = startCol; col <= endCol; col += 1) {
        newGridRow[startRow][col].nodeType = FloorGridNodeType.Wall;
        newGridRow[endRow][col].nodeType = FloorGridNodeType.Wall;
      }
      // Wall of rows
      for (let row = startRow; row <= endRow; row += 1) {
        newGridRow[row][startCol].nodeType = FloorGridNodeType.Wall;
        newGridRow[row][endCol].nodeType = FloorGridNodeType.Wall;
      }
      // Remove Highlight
      newGridRow[initialRowSelectedIndex][
        initialColSelectedIndex
      ].highlight = false;
      setRectangleStartIndex(undefined);
      setGridNodes(newGridRow);
    }
  };

  const onToolDeselect = () => {
    if (rectangleStartIndex) {
      // Remove Highlight
      const [row, col] = rectangleStartIndex;
      const newGrid = [...gridNodes];
      newGrid[row][col].highlight = false;
      setGridNodes(newGrid);
      setRectangleStartIndex(undefined);
    }
  };

  return {
    nodeSelected,
    deselectTool: onToolDeselect,
  };
};

export default useRectangleTool;
