import { GridNodeProps } from "../../../components/floor_plan_grid";

export type UseGridActionHookOutput = {
  nodeSelected: (row: number, col: number) => void;
  deselectTool: () => void;
};

export type UseGridActionHook = (
  gridNodes: Array<Array<GridNodeProps>>,
  setGridNodes: React.Dispatch<React.SetStateAction<GridNodeProps[][]>>
) => UseGridActionHookOutput;

export interface UseGridWaypointActionHookOutput {
  setWaypoint: (row: number, col: number, title: string) => void;
  removeWaypoint: (row: number, col: number) => void;
}

export type UseGridWaypointActionHook = (
  gridNodes: Array<Array<GridNodeProps>>,
  setGridNodes: React.Dispatch<React.SetStateAction<GridNodeProps[][]>>
) => UseGridWaypointActionHookOutput;
