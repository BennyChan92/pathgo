import { useState } from "react";
import { FloorGridNodeType } from "../../../service/api/types";
import { getLineIndexBetween } from "../util";
import { UseGridActionHook } from "./type";

const useLineTool: UseGridActionHook = (gridNodes, setGridNodes) => {
  const [lineStartIndex, setLineStartIndex] = useState<
    undefined | [number, number]
  >();

  const nodeSelected = (row: number, col: number) => {
    if (lineStartIndex === undefined) {
      const newGridRow = [...gridNodes];
      newGridRow[row][col].highlight = true;
      setGridNodes(newGridRow);
      setLineStartIndex([row, col]);
    } else {
      // Get New State for Path Nodes
      const [startIndexRow, startIndexCol] = lineStartIndex;
      const newGridRow = [...gridNodes];
      const path = getLineIndexBetween({
        array: gridNodes,
        start: [startIndexRow, startIndexCol],
        end: [row, col],
      });
      path.forEach((pathNode) => {
        const [pathRow, pathCol] = pathNode;
        switch (newGridRow[pathRow][pathCol].nodeType) {
          case FloorGridNodeType.Path:
            newGridRow[pathRow][pathCol].nodeType = FloorGridNodeType.Wall;
            break;
          case FloorGridNodeType.Wall:
            newGridRow[pathRow][pathCol].nodeType = FloorGridNodeType.Path;
            break;
          case FloorGridNodeType.Waypoint:
            newGridRow[pathRow][pathCol].nodeType = FloorGridNodeType.Wall;
            break;
          default:
        }
      });
      // Remove Highlight
      newGridRow[startIndexRow][startIndexCol].highlight = false;
      // Set State
      setGridNodes(newGridRow);
      setLineStartIndex(undefined);
    }
  };

  const onToolDeselect = () => {
    if (lineStartIndex) {
      // Remove Highlight
      const [row, col] = lineStartIndex;
      const newGrid = [...gridNodes];
      newGrid[row][col].highlight = false;
      setGridNodes(newGrid);
      setLineStartIndex(undefined);
    }
  };

  return {
    nodeSelected,
    deselectTool: onToolDeselect,
  };
};

export default useLineTool;
