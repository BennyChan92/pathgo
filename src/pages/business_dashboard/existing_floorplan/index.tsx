import { makeStyles } from "@material-ui/core";
import React from "react";
import Text from "../../../components/Text";
import { ThemeColor } from "../../../constant/theme";

export type BusinessFloorPlanProps = {
  businessName: string;
  address: string;
  onClick: () => void;
};

const useStyles = makeStyles({
  listItem: {
    boxSizing: "border-box",
    listStyleType: "none",
    border: "2px solid #3D5A80",
    borderRadius: "10px",
    backgroundColor: "#98C1D9",
    marginBottom: "18px",
    padding: "5px",
    "&:hover": {
      cursor: "pointer",
    },
  },
  businessTitle: {
    color: ThemeColor.Primary,
    fontSize: 24,
    fontWeight: "bold",
    paddingBottom: "12px",
  },
  address: {
    color: ThemeColor.Black,
    fontWeight: "lighter",
    opacity: 0.85,
  },
});

const BusinessFloorPlan: React.FC<BusinessFloorPlanProps> = ({
  businessName,
  address,
  onClick,
}) => {
  const classes = useStyles();
  return (
    <li className={classes.listItem} onClick={onClick}>
      <Text className={classes.businessTitle}>{businessName}</Text>
      <Text className={classes.address}>{address}</Text>
    </li>
  );
};

export default BusinessFloorPlan;
