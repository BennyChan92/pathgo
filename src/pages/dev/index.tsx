import { makeStyles } from "@material-ui/core";
import React from "react";
import NavLinkRightIcon from "@material-ui/icons/ArrowRightAlt";
import Button from "../../components/button";
import label from "../home/label.constant";
import APIStatus from "../home/api_status";
import Tools from "../home/tools";

const SAMPLE_BUSINESS_ID = "gRrn9OVZbqSK6FKkCjWB";

const useNavLinkStyles = makeStyles({
  wrapper: {
    marginBlock: "20px",
  },
});

const NavLink: React.FC<{
  link: string;
}> = ({ link, children }) => {
  const classes = useNavLinkStyles();
  return (
    <div className={classes.wrapper}>
      <Button navLink={link} endIcon={<NavLinkRightIcon />}>
        {children}
      </Button>
    </div>
  );
};

const DevPage: React.FC = () => {
  return (
    <div>
      <h2>Routes</h2>
      <NavLink link="/auth/login">{label.ButtonLabel.Login}</NavLink>
      <NavLink link="/auth/signup">{label.ButtonLabel.SignUp}</NavLink>
      <NavLink link="/floorplan/create">
        {label.ButtonLabel.CreateFloorPlan}
      </NavLink>
      <NavLink link="/floorplan/search">
        {label.ButtonLabel.SearchFloorPlan}
      </NavLink>
      <NavLink link={`/floorplan/${SAMPLE_BUSINESS_ID}/edit`}>
        {label.ButtonLabel.EditFloorPlan}
      </NavLink>
      <NavLink link={`/pathfinder/${SAMPLE_BUSINESS_ID}/prompt`}>
        {label.ButtonLabel.PromptPathFinder}
      </NavLink>
      <NavLink
        link={`/pathfinder/${SAMPLE_BUSINESS_ID}/navigate?sourceWaypointId=dev_start_id&destinationWaypointId=dev_end_id`}
      >
        {label.ButtonLabel.PathFinderNav}
      </NavLink>
      <NavLink link="/dashboard">{label.ButtonLabel.BusinessDashboard}</NavLink>
      <NavLink link="/error/404">{label.ButtonLabel.Error404}</NavLink>
      <Tools />
      <h2>ETC</h2>
      <APIStatus />
    </div>
  );
};

export default DevPage;
