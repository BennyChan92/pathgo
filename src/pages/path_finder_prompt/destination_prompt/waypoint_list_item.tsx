import { makeStyles } from "@material-ui/core";
import React from "react";
import { ThemeColor } from "../../../constant/theme";

export type WaypointListItemProps = {
  title: string;
};

const useStyles = makeStyles({
  listItem: {
    boxSizing: "border-box",
    borderRadius: "8px",
    border: `1px solid ${ThemeColor.Dark}`,
    textAlign: "center",
    width: "100%",
    display: "block",
    marginTop: "12px",
    backgroundColor: ThemeColor.Info,
    padding: "6px",
    "&:hover": {
      cursor: "pointer",
    },
    color: ThemeColor.Black,
  },
});

const WaypointListItem: React.FC<WaypointListItemProps> = ({ title }) => {
  const classes = useStyles();
  return <div className={classes.listItem}>{title}</div>;
};

export default WaypointListItem;
