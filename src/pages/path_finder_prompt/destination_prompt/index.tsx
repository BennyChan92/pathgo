import { makeStyles } from "@material-ui/core";
import React, { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router-dom";
import TextFieldComponent from "../../../components/form/textfield";
import useFormChange from "../../../components/form/textfield/useFormChange";
import LinearProgress from "../../../components/linear_progress";
import FloorPlanService from "../../../service/api/floorplan";
import { WaypointSummary } from "../../../service/api/types";
import WaypointListItem from "./waypoint_list_item";

export type DestinationPromptProps = {
  onSelectDestination: (row: number, column: number) => void;
};

const useStyles = makeStyles({
  listContainer: {},
  header: {
    textAlign: "center",
  },
  listLink: {
    textDecoration: "none",
  },
});

const DestinationPrompt: React.FC<DestinationPromptProps> = ({
  onSelectDestination,
}) => {
  const { businessId } = useParams<{
    businessId: string;
  }>();
  const classes = useStyles();
  const [filterWPTitle, handleChangeFilterWPTitle] = useFormChange();
  const [waypoints, setWaypoints] = useState<Array<WaypointSummary>>([]);
  const [loadingWaypoints, setLoadingWaypoints] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string>();

  useEffect(() => {
    const retrieveWaypoints = async () => {
      try {
        if (errorMessage) setErrorMessage(undefined);
        setLoadingWaypoints(true);
        const result = await FloorPlanService.GetWaypointsForBusiness(
          businessId
        );
        setWaypoints(result);
        setLoadingWaypoints(false);
      } catch (error) {
        setErrorMessage(error.message || "Unable to retrieve waypoints");
        setLoadingWaypoints(false);
      }
    };

    retrieveWaypoints();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [businessId]);

  const wayPointView = useMemo(() => {
    let filteredWP = waypoints;
    if (filterWPTitle.length) {
      filteredWP = filteredWP.filter(
        (entry) =>
          entry.title.toLowerCase().indexOf(filterWPTitle.toLowerCase()) !== -1
      );
    }
    // TODO: Set Nav Link to Where are you page
    return filteredWP.map((entry) => (
      <div
        key={entry.title}
        onClick={() => onSelectDestination(entry.row, entry.column)}
      >
        <WaypointListItem title={entry.title} />
      </div>
    ));
  }, [filterWPTitle, onSelectDestination, waypoints]);

  const filterTextFieldView = useMemo(
    () => (
      <TextFieldComponent
        value={filterWPTitle}
        onChange={handleChangeFilterWPTitle}
        placeholder="Filter Search..."
        fullWidth
      />
    ),
    [filterWPTitle, handleChangeFilterWPTitle]
  );

  return (
    <div>
      <h1 className={classes.header}>Where are you going?</h1>
      <div className={classes.listContainer}>
        {filterTextFieldView}
        {loadingWaypoints && <LinearProgress />}
        {wayPointView}
      </div>
    </div>
  );
};

export default DestinationPrompt;
