import { makeStyles } from "@material-ui/core";
import React, { useEffect, useMemo, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import CurrentLocationPrompt from "./current_location_prompt";
import DestinationPrompt from "./destination_prompt";
import Stepper, { Step, StepLabel } from "../../components/stepper";
import AppStorage from "../../service/local_storage";
import SimpleNavbarLayout from "../../components/layout/simple/navbar";

const useStyles = makeStyles({
  root: {
    display: "flex",
    // alignItems: "center",
    flexDirection: "column",
  },
  step: {},
  stepper: {
    alignItems: "center",
    width: "100%",
  },
  stepperActionWrapper: {
    marginTop: "16px",
    display: "flex",
    alignItems: "end",
  },
  stepperActionButton: {
    marginLeft: "auto",
  },
  destinationPrompt: {
    width: "80vw",
    marginLeft: "auto",
    marginRight: "auto",
  },
  locationPrompt: {
    width: "100vw",
  },
});

const PathFinderPromptScreen: React.FC = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [userDestination, setUserDestination] = useState<[number, number]>();
  const { businessId } = useParams<{ businessId: string }>();
  const history = useHistory();

  useEffect(() => {
    AppStorage.AddToRecentlyVisited(businessId);
  }, [businessId]);

  const stepperView = useMemo(
    () => (
      <div className={classes.stepper}>
        <Stepper activeStep={activeStep} alternativeLabel>
          <Step className={classes.step} onClick={() => setActiveStep(0)}>
            <StepLabel>Destination</StepLabel>
          </Step>
          <Step className={classes.step} onClick={() => setActiveStep(1)}>
            <StepLabel>Current Location</StepLabel>
          </Step>
        </Stepper>
      </div>
    ),
    [activeStep, classes.step, classes.stepper]
  );

  const stepContentView = useMemo(() => {
    const nextStep = () => {
      if (activeStep !== 1) setActiveStep(activeStep + 1);
    };

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const handleSelectDestination = (row: number, column: number) => {
      setUserDestination([row, column]);
      nextStep();
    };

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const handleSelectLocation = (startRow: number, startCol: number) => {
      const [endRow, endCol] = userDestination || [0, 0];
      history.push(
        `/pathfinder/${businessId}/navigate?startRow=${startRow}&startCol=${startCol}&endRow=${endRow}&endCol=${endCol}`
      );
    };

    if (activeStep === 0) {
      return (
        <div className={classes.destinationPrompt}>
          <DestinationPrompt onSelectDestination={handleSelectDestination} />
        </div>
      );
    }
    return (
      <div className={classes.locationPrompt}>
        <CurrentLocationPrompt
          onLocationSelect={handleSelectLocation}
          businessId={businessId}
        />
      </div>
    );
  }, [
    activeStep,
    businessId,
    classes.destinationPrompt,
    classes.locationPrompt,
    history,
    userDestination,
  ]);

  return (
    <SimpleNavbarLayout>
      <div className={classes.root}>
        {stepperView}
        {stepContentView}
      </div>
    </SimpleNavbarLayout>
  );
};

export default PathFinderPromptScreen;
