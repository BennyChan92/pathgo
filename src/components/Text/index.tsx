import Typography from "@material-ui/core/Typography";
import React, { useMemo } from "react";

export type TextProps = {
  // ===== Mutually Exclusive =====
  alignRight?: boolean;
  alignLeft?: boolean;
  center?: boolean;
  // ==============================
  paragraph?: boolean;
  header?: 1 | 2 | 3 | 4 | 5 | 6;
  subtitle?: boolean;
  className?: string;
  color?: string;
};

const Text: React.FC<TextProps> = ({
  alignLeft,
  alignRight,
  center,
  paragraph,
  header,
  subtitle,
  className = "",
  color,
  children,
}) => {
  const alignProp = useMemo(() => {
    if (alignLeft) return "left";
    if (alignRight) return "right";
    if (center) return "center";
    return "inherit";
  }, [alignLeft, alignRight, center]);

  const variantProp = useMemo(() => {
    if (subtitle) return "h6";
    switch (header) {
      case 1:
        return "h1";
      case 2:
        return "h2";
      case 3:
        return "h3";
      case 4:
        return "h4";
      case 5:
        return "h5";
      case 6:
        return "h6";
      default:
        return "body1";
    }
  }, [header, subtitle]);

  const styles: React.CSSProperties = useMemo(() => {
    return {
      color,
    };
  }, [color]);

  return (
    <Typography
      variant={variantProp}
      style={styles}
      align={alignProp}
      paragraph={paragraph}
      className={className}
    >
      {children}
    </Typography>
  );
};

export default Text;
