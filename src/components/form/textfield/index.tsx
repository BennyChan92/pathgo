import React from "react";
import MTextField from "@material-ui/core/TextField";

export enum TextFieldVariant {
  Outlined = "outlined",
  Filled = "filled",
  Standard = "standard",
}

// Specifies which props are allowed
export type TextFieldProps = {
  required?: boolean;
  error?: boolean;
  fullWidth?: boolean;
  disabled?: boolean;
  select?: boolean;
  placeholder?: string;
  autoComplete?: string;
  type?: React.InputHTMLAttributes<HTMLInputElement>["type"];
  label?: React.ReactNode;
  helperText?: React.ReactNode;
  variant?: TextFieldVariant;
  // color?: ThemeColor;
  onChange?: (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  value?: string | number;
  className?: string;
};

const TextField: React.FC<TextFieldProps> = ({
  required,
  error,
  fullWidth,
  disabled = false,
  placeholder,
  autoComplete,
  label,
  helperText,
  type,
  select,
  className,
  variant,
  // color=ThemeColor.Primary,
  onChange,
  value,
  children,
}) => {
  const textField = (
    <MTextField
      select={select}
      className={className}
      placeholder={placeholder}
      autoComplete={autoComplete}
      label={label}
      helperText={helperText}
      disabled={disabled}
      error={error}
      required={required}
      fullWidth={fullWidth}
      type={type}
      variant={variant}
      size="small"
      // color={color}
      onChange={onChange}
      value={value}
    >
      {children}
    </MTextField>
  );
  return textField;
};

export default TextField;
