import React, { useMemo } from "react";
import {
  StandardAuthUserNavItems,
  StandardReguarUserNavItems,
} from "../../../config/navbar";
import { useIsAuthenticated } from "../../../hooks/user";
import NavBar from "../../navbar";

const SimpleNavbarLayout: React.FC = ({ children }) => {
  const isLoggedIn = useIsAuthenticated();

  const navbarView = useMemo(() => {
    return (
      <NavBar
        navItems={
          isLoggedIn ? StandardAuthUserNavItems : StandardReguarUserNavItems
        }
      />
    );
  }, [isLoggedIn]);

  return (
    <div>
      {navbarView}
      {children}
    </div>
  );
};

export default SimpleNavbarLayout;
