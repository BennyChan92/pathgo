import {
  Container,
  Grid,
  makeStyles,
  MenuItem,
  Paper,
  Typography,
} from "@material-ui/core";
import React, { useMemo } from "react";
import cx from "classnames";
import { ThemeColor } from "../../constant/theme/index";
import TextField from "../form/textfield";

export type BusinessListItemProps = {
  businessName: string;
  address: string;
  zipcode: number;
  id: string;
  onClick?: () => void;
  actions?: BusinessListItemActionProp;
};

const useStyles = makeStyles({
  listItem: {
    boxSizing: "border-box",
    listStyleType: "none",
    border: "2px solid #3D5A80",
    borderRadius: "10px",
    backgroundColor: "#98C1D9",
    marginBottom: "18px",
    padding: "5px",
    "&:hover": {
      cursor: "pointer",
    },
  },
  businessTitle: {
    color: ThemeColor.Primary,
    fontSize: 24,
    fontWeight: "bold",
  },
  address: {
    color: ThemeColor.Black,
    fontSize: 18,
    fontWeight: "lighter",
    opacity: 0.85,
  },
  root: {
    flexGrow: 1,
    paddingTop: "10px",
  },
  paper: {
    backgroundColor: ThemeColor.Light,

    padding: "10px",
  },
  clickable: {
    "&:hover": {
      cursor: "pointer",
    },
  },
});

export type BusinessListItemActionProp = {
  onSelect: (actionName: string) => void;
  items: Set<string>;
};

type HeaderProps = {
  businessName: string;
  actions?: BusinessListItemActionProp;
};

const Header: React.FC<HeaderProps> = ({ businessName, actions }) => {
  const classes = useStyles();

  const actionView = useMemo(
    () =>
      actions ? (
        <TextField
          fullWidth
          select
          label="Action"
          onChange={(e) => actions.onSelect(e.target.value)}
        >
          {Array.from(actions.items).map((action) => (
            <MenuItem key={action} value={action}>
              {action}
            </MenuItem>
          ))}
        </TextField>
      ) : (
        <></>
      ),
    []
  );

  return (
    <Grid container>
      <Grid item xs={actions ? 10 : 12}>
        <h2 className={classes.businessTitle}>{businessName}</h2>
      </Grid>
      {actions && (
        <Grid item xs={2}>
          {actionView}
        </Grid>
      )}
    </Grid>
  );
};

const BusinessListItem: React.FC<BusinessListItemProps> = ({
  businessName,
  address,
  zipcode,
  onClick,
  actions,
}) => {
  const classes = useStyles();

  const rootStyle = cx({
    [classes.root]: true,
    [classes.clickable]: onClick,
  });

  return (
    <div className={rootStyle} onClick={onClick}>
      <Paper className={classes.paper}>
        <Container maxWidth="xl">
          <Grid container direction="column" spacing={0}>
            <Grid item xs={12}>
              <Header businessName={businessName} actions={actions} />
            </Grid>
            <Grid item xs={12}>
              <Typography
                className={classes.address}
                variant="body2"
                gutterBottom
              >
                {address} {zipcode}
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </Paper>
    </div>
  );
};

export default BusinessListItem;
