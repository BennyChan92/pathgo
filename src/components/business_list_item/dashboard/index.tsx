import { makeStyles } from "@material-ui/core";
import React, { useMemo, useState } from "react";
import { useHistory } from "react-router-dom";
import { ThemeColor } from "../../../constant/theme/index";
import { useErrorAlert } from "../../../hooks/alert";
import BusinessService from "../../../service/api/business";
import LinearProgress from "../../linear_progress";
import BusinessListItem, {
  BusinessListItemActionProp,
} from "../BusinessListItem";

export type DashBusinessListItemProps = {
  businessName: string;
  address: string;
  zipcode: number;
  id: string;
  onDelete: () => void;
};
const useStyles = makeStyles({
  listItem: {
    boxSizing: "border-box",
    listStyleType: "none",
    border: "2px solid #3D5A80",
    borderRadius: "10px",
    backgroundColor: "#98C1D9",
    marginBottom: "18px",
    padding: "5px",
    "&:hover": {
      cursor: "pointer",
    },
  },
  businessTitle: {
    color: ThemeColor.Primary,
    fontSize: 24,
    fontWeight: "bold",
    paddingBottom: "12px",
  },
  address: {
    color: ThemeColor.Black,
    fontSize: 18,
    fontWeight: "lighter",
    opacity: 0.85,
  },
  root: {
    flexDirection: "row",
    flexGrow: 1,
    paddingTop: "10px",
  },

  paper: {
    backgroundColor: ThemeColor.Light,
    "&:hover": {
      cursor: "pointer",
    },
    padding: "10px",
  },
  image: {
    width: 128,
    height: 128,
  },
  img: {
    margin: "auto",
    display: "block",
    maxWidth: "100%",
    maxHeight: "100%",
  },
});

const DashBusinessListItem: React.FC<DashBusinessListItemProps> = ({
  businessName,
  address,
  zipcode,
  onDelete,
  id,
}) => {
  const classes = useStyles();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const showError = useErrorAlert();

  const actionProps = useMemo(() => {
    const actions: Record<string, () => void> = {
      Edit: () => {
        history.push(`/floorplan/${id}/edit`);
      },
      "QR Code": () => {
        history.push(`/qr/generate?businessId=${id}`);
      },
      Delete: async () => {
        try {
          setLoading(true);
          await BusinessService.DeleteBusiness(id);
          onDelete();
        } catch (error) {
          showError(error.message || "Unable to delete");
          setLoading(false);
        }
      },
    };

    const prop: BusinessListItemActionProp = {
      onSelect: (name) => {
        actions[name]();
      },
      items: new Set(Object.keys(actions)),
    };
    return prop;
  }, []);

  return (
    <div className={classes.root}>
      {loading && <LinearProgress />}
      <BusinessListItem
        id={id}
        businessName={businessName}
        address={address}
        zipcode={zipcode}
        actions={actionProps}
      />
    </div>
  );
};

export default DashBusinessListItem;
