import React, { useMemo, useState } from "react";
import Snackbar, { SnackbarProps } from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

export enum AlertType {
  Success = "success",
  Error = "error",
  Warning = "warning",
  Info = "info",
}

export type AlertProps = {
  isOpen?: boolean;
  closeAlert: () => void;
  type?: AlertType;
  autohide?: boolean;
};

export type UseAlertStateHook = () => {
  isOpen: boolean;
  openAlert: (message?: string, type?: AlertType) => void;
  closeAlert: () => void;
  alertMessage: string;
  setAlertMessage: (message: string) => void;
  alertType: AlertType;
  setAlertType: (type: AlertType) => void;
};

export const useAlertState: UseAlertStateHook = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [alertType, setAlertType] = useState(AlertType.Info);

  const openAlert = (message?: string, type?: AlertType) => {
    if (message) {
      setAlertMessage(message);
    }
    if (type) {
      setAlertType(type);
    }
    setIsOpen(true);
  };

  const closeAlert = () => setIsOpen(false);

  return {
    isOpen,
    openAlert,
    closeAlert,
    alertMessage,
    setAlertMessage,
    alertType,
    setAlertType,
  };
};

const Alert: React.FC<AlertProps> = ({
  closeAlert,
  type = AlertType.Info,
  isOpen,
  autohide,
  children,
}) => {
  const alert = useMemo(
    () => (
      <MuiAlert severity={type} onClose={closeAlert}>
        {children}
      </MuiAlert>
    ),
    [children, closeAlert, type]
  );

  if (isOpen !== undefined) {
    const snackbarProps: SnackbarProps = {
      open: isOpen,
      onClose: closeAlert,
    };
    if (autohide) snackbarProps.autoHideDuration = 3000;
    return (
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...snackbarProps}
      >
        {alert}
      </Snackbar>
    );
  }

  return alert;
};

export default Alert;
