/* eslint-disable react/jsx-props-no-spreading */
import { makeStyles } from "@material-ui/core";
import MStep, { StepProps } from "@material-ui/core/Step";
import MStepLabel, { StepLabelProps } from "@material-ui/core/StepLabel";
import MStepper, { StepperProps } from "@material-ui/core/Stepper";
import React from "react";
import { ThemeColor } from "../../constant/theme";

const stepperStyles = makeStyles({
  stepper: {},
});

const Stepper: React.FC<StepperProps> = (props) => {
  const classes = stepperStyles();
  return <MStepper className={classes.stepper} {...props} />;
};

const stepStyles = makeStyles({
  root: {
    "& span.MuiStepLabel-iconContainer.MuiStepLabel-alternativeLabel > svg": {
      color: ThemeColor.Black,
    },
    "& span > span.MuiStepLabel-iconContainer.MuiStepLabel-alternativeLabel > svg.MuiStepIcon-active": {
      color: ThemeColor.Secondary,
    },
  },
  completed: {
    "& svg": {
      color: `${ThemeColor.Success} !important`,
    },
  },
});

export const Step: React.FC<StepProps> = (props) => {
  const classes = stepStyles();
  return (
    <MStep
      classes={{ root: classes.root, completed: classes.completed }}
      {...props}
    />
  );
};

export const StepLabel: React.FC<StepLabelProps> = (props) => {
  return <MStepLabel {...props} />;
};

export default Stepper;
