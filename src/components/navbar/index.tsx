import React, { useMemo } from "react";
import AppBar from "@material-ui/core/AppBar";
import { makeStyles, Toolbar } from "@material-ui/core";
import cx from "classnames";
import { NavLink, useHistory } from "react-router-dom";
import { ThemeColor } from "../../constant/theme";
import Text from "../Text";
import Logo from "../../assets/images/logo/PathGoLogoLight.png";

export type NavbarColor =
  | ThemeColor.Primary
  | ThemeColor.Secondary
  | ThemeColor.Info;

export type NavbarNavItem = {
  title: string;
  link: string; // internal nav
};

/**
 * This will be a reusable Navbar throughout the App
 * Example: This should be reusable for homepage as well
 * as Floor Plan Edit Page
 */
export type NavbarProps = {
  title?: string;
  color?: NavbarColor;
  right?: React.ReactElement; // Exclusive with navItems
  navItems?: Array<NavbarNavItem>; // Exclusive with Right
};

const useStyles = makeStyles({
  grow: {
    flexGrow: 1,
  },
  navbar: {
    flexGrow: 1,
  },
  primary: {
    backgroundColor: ThemeColor.Primary,
  },
  secondary: {
    backgroundColor: ThemeColor.Secondary,
  },
  info: {
    backgroundColor: ThemeColor.Info,
  },
  navItemButton: {
    marginRight: "20px",
  },
  navLink: {
    padding: "16px",
    textDecoration: "none",
    fontSize: "16px",
    color: ThemeColor.White,
    fontWeight: 600,
    "&:hover": {
      opacity: 0.8,
    },
  },
  activeNavlink: {
    color: ThemeColor.Light,
  },
  defaultTitleWrapper: {
    padding: "16 px 16px 16px 0",
    "&:hover": {
      cursor: "pointer",
    },
  },
  logo: {
    height: "30px",
  },
});

const NavBar: React.FC<NavbarProps> = ({
  title,
  right,
  navItems,
  color = ThemeColor.Primary,
}) => {
  // TODO: Style this to our theme's styles
  const classes = useStyles();
  const history = useHistory();

  const navbarClassName = useMemo(
    () =>
      cx({
        [classes.navbar]: true,
        [classes.primary]: color === ThemeColor.Primary,
        [classes.secondary]: color === ThemeColor.Secondary,
        [classes.info]: color === ThemeColor.Info,
      }),
    [classes.navbar, classes.primary, classes.secondary, classes.info, color]
  );

  const titleColor = useMemo(() => {
    if (color === ThemeColor.Secondary || color === ThemeColor.Info)
      return ThemeColor.Dark;
    return ThemeColor.Light;
  }, [color]);

  const rightNavView = useMemo(() => {
    if (right) return right;

    return navItems?.map((item, key) => (
      <NavLink
        // eslint-disable-next-line react/no-array-index-key
        key={key}
        to={item.link}
        className={classes.navLink}
        exact
        activeClassName={classes.activeNavlink}
      >
        {item.title}
      </NavLink>
    ));
  }, [classes.activeNavlink, classes.navLink, navItems, right]);

  const titleView = useMemo(() => {
    if (title) {
      return (
        <Text subtitle color={titleColor}>
          {title}
        </Text>
      );
    }
    return (
      <div
        className={classes.defaultTitleWrapper}
        onClick={() => {
          history.push("/");
        }}
      >
        <img className={classes.logo} src={Logo} alt="Website Logo" />
      </div>
    );
  }, [classes.defaultTitleWrapper, classes.logo, history, title, titleColor]);

  return (
    <AppBar position="static" className={navbarClassName}>
      <Toolbar>
        {titleView}
        <div className={classes.grow} />
        {rightNavView}
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
