import { makeStyles } from "@material-ui/core";
import { ThemeColor } from "../../constant/theme";

const useStyles = makeStyles({
  wrapper: {
    background: ThemeColor.Info,
    margin: 0,
    padding: 0,
    minHeight: "100%",
    // overflow: "auto",
    overflowY: "auto",
    "&::-webkit-scrollbar": {
      width: 0,
    },
  },
  node: {
    // outline: `1px solid ${ThemeColor.Dark}`,
    boxSizing: "border-box",
    display: "inline-block",
    minWidth: "20px",
    maxWidth: "20px",
    minHeight: "20px",
    maxHeight: "20px",
  },
  editableNode: {
    "&:hover": {
      backgroundColor: ThemeColor.Light,
      opacity: 0.4,
    },
  },
  wallNode: {
    backgroundColor: ThemeColor.Light,
  },
  pathNode: {
    backgroundColor: ThemeColor.Primary,
  },
  navPathNode: {
    backgroundColor: ThemeColor.Secondary,
  },
  highlightNode: {
    backgroundColor: ThemeColor.Light,
    opacity: 0.6,
  },
  waypointNode: {
    left: 0,
    top: 0,
    backgroundColor: ThemeColor.Primary,
    "&:hover": {
      cursor: "pointer",
      opacity: 1,
      backgroundColor: ThemeColor.Primary,
    },
  },
  waypointIcon: {
    boxSizing: "border-box",
    height: "100%",
    width: "100%",
  },
  waypointTitle: {
    zIndex: 1,
    overflow: "visible",
  },
  locationIcon: {
    boxSizing: "border-box",
    height: "100%",
    width: "100%",
  },
});

export default useStyles;
