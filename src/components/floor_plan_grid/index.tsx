import React, { Fragment, useMemo } from "react";
import WaypointIcon from "@material-ui/icons/LocationOn";
import NearMeIcon from "@material-ui/icons/NearMe";
import cx from "classnames";
import {
  FloorGridNodeType,
  FloorPlanGridNode,
  WaypointGridNodeProps,
  LocationGridNodeProps,
} from "../../service/api/types";
import Tooltip from "../tooltip";
import NavigationPathList from "./nav_path_list";
import useStyles from "./styles";

export type GridWaypointNodeProps = WaypointGridNodeProps;

export interface GridNodeProps extends FloorPlanGridNode {
  highlight: boolean;
  nodeProps?: GridWaypointNodeProps;
}

export type FloorPlanGridProps = {
  gridNodes: Array<Array<GridNodeProps>>;
  onNodeSelected?: (row: number, col: number) => void;
  editable?: boolean;
  userLocation?: LocationGridNodeProps;
  navPathNodes?: Array<LocationGridNodeProps>;
};

const FloorPlanGrid: React.FC<FloorPlanGridProps> = ({
  gridNodes,
  onNodeSelected,
  editable = true,
  userLocation,
  navPathNodes,
}) => {
  const classes = useStyles();

  const navPathList = useMemo(
    () => new NavigationPathList(navPathNodes || []),
    [navPathNodes]
  );

  const renderNode = (row: number, col: number, nodeProp: GridNodeProps) => {
    const nodeClasses = cx({
      [classes.node]: true,
      [classes.editableNode]: editable,
      [classes.wallNode]: nodeProp.nodeType === FloorGridNodeType.Wall,
      [classes.pathNode]: nodeProp.nodeType === FloorGridNodeType.Path,
      [classes.waypointNode]: nodeProp.nodeType === FloorGridNodeType.Waypoint,
      [classes.highlightNode]: nodeProp.highlight,
      [classes.navPathNode]: navPathList.has(row, col),
    });
    let nodeRender;
    if (userLocation?.row === row && userLocation?.col === col) {
      nodeRender = <NearMeIcon className={classes.locationIcon} />;
    } else
      nodeRender = nodeProp.nodeType === FloorGridNodeType.Waypoint && (
        <>
          <Tooltip title={nodeProp.nodeProps?.title || "Untitled"} alwaysOpen>
            <WaypointIcon className={classes.waypointIcon} />
          </Tooltip>
        </>
      );
    return (
      // eslint-disable-next-line
      <>
        <div
          className={nodeClasses}
          onClick={() => {
            if (onNodeSelected) onNodeSelected(row, col);
          }}
        >
          {nodeRender}
        </div>
      </>
    );
  };

  return (
    <div className={classes.wrapper}>
      <div>
        {gridNodes.map((rowNodes, rowIndex) => (
          <div
            // eslint-disable-next-line react/no-array-index-key
            key={rowIndex}
            style={{
              display: "flex",
              flexDirection: "row",
              flexWrap: "nowrap",
            }}
          >
            {rowNodes.map((nodeProp, colIndex) => (
              // eslint-disable-next-line react/no-array-index-key
              <Fragment key={`${rowIndex}-${colIndex}`}>
                {renderNode(rowIndex, colIndex, nodeProp)}
              </Fragment>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
};

export default FloorPlanGrid;
