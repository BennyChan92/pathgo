import { fireEvent, render } from "@testing-library/react";
import React from "react";
import { act } from "react-dom/test-utils";
import Button from ".";

describe("Test Button Component", () => {
  it("Renders children presented inside button", () => {
    const expectedChildText = "Hello World";
    const { getByText } = render(<Button>{expectedChildText}</Button>);
    expect(getByText(expectedChildText)).toBeTruthy();
  });

  it("Calls onClick function when button is pressed", async () => {
    const expectedChildText = "Hello World";
    const mockOnPressHandler = jest.fn();
    const { getByText } = render(
      <Button onClick={mockOnPressHandler}>{expectedChildText}</Button>
    );
    act(() => {
      fireEvent.click(getByText(expectedChildText));
    });
    expect(mockOnPressHandler).toBeCalledTimes(1);
  });

  it("Doesn't Calls onClick function if disabled", async () => {
    const expectedChildText = "Hello World";
    const mockOnPressHandler = jest.fn();
    const { getByText } = render(
      <Button onClick={mockOnPressHandler} disabled>
        {expectedChildText}
      </Button>
    );
    act(() => {
      fireEvent.click(getByText(expectedChildText));
    });
    expect(mockOnPressHandler).toBeCalledTimes(0);
  });
});
