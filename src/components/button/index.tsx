import React, { useMemo } from "react";
import MButton from "@material-ui/core/Button";
import classnames from "classnames";
import { Link } from "react-router-dom";
import { ThemeColor } from "../../constant/theme";
import useStyles from "./styles";

export enum ButtonSize {
  Small = "small",
  Medium = "medium",
  Large = "large",
}

export enum ButtonVariant {
  Contained = "contained",
  Outlined = "outlined",
  Text = "text",
}

export type ButtonProps = {
  color?: ThemeColor;
  disabled?: boolean;
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
  fullWidth?: boolean;
  size?: ButtonSize;
  variant?: ButtonVariant;
  navLink?: string;
  onClick?: React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  >["onClick"];
  className?: string;
};

// Which colors from ButtonProps.color should use white texts
const WhiteTextColorSet = new Set([ThemeColor.Primary, ThemeColor.Black]);

const Button: React.FC<ButtonProps> = ({
  disabled = false,
  color = ThemeColor.Primary,
  variant = ButtonVariant.Contained,
  className,
  startIcon,
  endIcon,
  fullWidth,
  size,
  navLink,
  onClick,
  children,
}) => {
  const classes = useStyles();

  const buttonClassName = useMemo(
    () =>
      classnames(
        {
          [classes.whiteText]: WhiteTextColorSet.has(color),
        },
        className
      ),
    [className, classes.whiteText, color]
  );

  const button = (
    <MButton
      className={buttonClassName}
      style={{ backgroundColor: color }}
      disabled={disabled}
      startIcon={startIcon}
      endIcon={endIcon}
      fullWidth={fullWidth}
      size={size}
      variant={variant}
      onClick={onClick}
    >
      {children}
    </MButton>
  );

  if (navLink) {
    return (
      <Link className={classes.navlink} to={navLink}>
        {button}
      </Link>
    );
  }

  return button;
};

export default Button;
