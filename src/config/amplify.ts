export const GetENVOrThrow = (varname: string): string => {
  if (process.env[varname] === undefined)
    throw new Error(`Missing Environment Variable: ${varname}`);
  return process.env[varname] as string;
};

export const AuthConfig = {
  region: GetENVOrThrow("REACT_APP_USER_POOL_REGION"),
  userPoolId: GetENVOrThrow("REACT_APP_USER_POOL_ID"),
  userPoolWebClientId: GetENVOrThrow("REACT_APP_USER_POOL_CLIENT_ID"),
};
