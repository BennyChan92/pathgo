import axios from "axios";
import AppStorage from "../local_storage";

type AddressComponent = {
  long_name: string;
  short_name: string;
  types: string[];
};

export default class Geocode {
  static async GetZipCode(
    latitude: number,
    longitude: number
  ): Promise<number | undefined> {
    if (process.env.REACT_APP_GEOCODE_API_KEY === undefined) {
      throw new Error("Google API Key Not Defined");
    }
    const cachedZipcode = AppStorage.GetZipcode(latitude, longitude);
    if (cachedZipcode) {
      return cachedZipcode;
    }
    const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${process.env.REACT_APP_GEOCODE_API_KEY}`;
    const response = await axios.get(url);
    const components = response.data.results[0]
      .address_components as AddressComponent[];
    const entry = components.find(
      (comp) => comp.types.indexOf("postal_code") !== -1
    );
    if (entry) {
      const zipcode = parseInt(entry.long_name, 10);
      AppStorage.SaveZipcode(latitude, longitude, zipcode);
      return zipcode;
    }
    return undefined;
  }
}
