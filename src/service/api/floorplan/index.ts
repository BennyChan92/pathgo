import { sortBy } from "lodash";
import pako from "pako";
import BusinessService from "../business";
import APIRequest from "../request";
import {
  FloorPlanModel,
  LocationGridNodeProps,
  WaypointSummary,
} from "../types";

type APIResponsePathBetween = {
  userpath: Array<[number, number]>; // [[row, col]]
};

export default class FloorPlanService {
  static async RetrieveFloorplanForBusiness(
    businessId: string
  ): Promise<FloorPlanModel> {
    const business = await BusinessService.FindById(businessId);
    return business.floorPlan;
  }

  static async SetFloorplanForBusiness(
    businessId: string,
    floorPlan: FloorPlanModel
  ): Promise<void> {
    const formattedFloorplan = pako.deflate(JSON.stringify(floorPlan));
    await APIRequest.post(`business/id/${businessId}/floorPlan`, {
      floorPlan: formattedFloorplan,
    });
  }

  static async GetWaypointsForBusiness(
    id: string
  ): Promise<Array<WaypointSummary>> {
    const waypoints: Array<WaypointSummary> = [];
    const floorPlan = await BusinessService.FindById(id);
    floorPlan.floorPlan[0].floorGrid.forEach((row, rowIndex) =>
      row.forEach((node, colIndex) => {
        if (node.nodeProps?.title)
          waypoints.push({
            title: node.nodeProps.title,
            row: rowIndex,
            column: colIndex,
          });
      })
    );
    return sortBy(waypoints, ["title"]);
  }

  static async GetDestinationPath(
    businessId: string,
    userLocation: LocationGridNodeProps,
    destionation: LocationGridNodeProps
  ): Promise<Array<LocationGridNodeProps>> {
    const url = `business/id/${businessId}/get-path?start_row=${userLocation.row}&start_col=${userLocation.col}&end_row=${destionation.row}&end_col=${destionation.col}`;
    const response = await APIRequest.get<APIResponsePathBetween>(url);
    const path: Array<LocationGridNodeProps> = response.userpath.map(
      (entry) => ({
        row: entry[0],
        col: entry[1],
      })
    );
    // eslint-disable-next-line no-console
    console.log(path);
    return path;
  }
}
