import axios from "axios";
import api from "../../../config/api";
import AuthService from "../../auth";

export default class APIRequest {
  static async get<T>(route: string): Promise<T> {
    try {
      const path = `${api.PublicAPIURL}/${route}`;
      const result = await axios.get(path);
      return result.data as T;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      if (axios.isAxiosError(error)) {
        throw new Error(
          error.response?.data?.error?.message ||
            "Something went wrong getting from the server"
        );
      }
      throw error;
    }
  }

  static async post<T>(
    route: string,
    data: Record<string, unknown>,
    authRoute = false
  ): Promise<T> {
    try {
      const path = `${
        authRoute ? api.AuthrizedAPIUrl : api.PublicAPIURL
      }/${route}`;
      const headers: Record<string, string> = {};
      if (authRoute) {
        const token = await AuthService.GetAuthroizationToken();
        headers.Authorization = token;
      }
      const result = await axios.post(path, data, {
        headers,
      });
      return result.data as T;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      if (axios.isAxiosError(error)) {
        throw new Error(
          error.response?.data?.error?.message ||
            "Something went wrong posting to the server"
        );
      }
      throw error;
    }
  }

  static async delete<T>(route: string, authRoute = false): Promise<T> {
    try {
      const path = `${
        authRoute ? api.AuthrizedAPIUrl : api.PublicAPIURL
      }/${route}`;
      const headers: Record<string, string> = {};
      if (authRoute) {
        const token = await AuthService.GetAuthroizationToken();
        headers.Authorization = token;
      }
      const result = await axios.delete(path, {
        headers,
      });
      return result.data as T;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      if (axios.isAxiosError(error)) {
        throw new Error(
          error.response?.data?.error?.message ||
            "Something went wrong deleting"
        );
      }
      throw error;
    }
  }
}
