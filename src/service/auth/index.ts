import { Auth } from "aws-amplify";
import { isString } from "lodash";

export type IUser = {
  id: string;
};

export type AuthServiceSignupProps = {
  email: string;
  password: string;
};

export default class AuthService {
  static async Login(email: string, password: string): Promise<IUser> {
    try {
      const user = await Auth.signIn(email, password);
      const userId = user.attributes.sub as string;
      return {
        id: userId,
      };
    } catch (error) {
      throw new Error(error.message);
    }
  }

  static async Signup(props: AuthServiceSignupProps): Promise<void> {
    try {
      await Auth.signUp({
        username: props.email,
        password: props.password,
        attributes: {
          email: props.email,
        },
      });
    } catch (error) {
      throw new Error(error.message);
    }
  }

  static async Logout(): Promise<void> {
    await Auth.signOut();
  }

  static async RestoreSavedUser(): Promise<IUser> {
    const user = await Auth.currentAuthenticatedUser();
    return user;
  }

  static async GetAuthroizationToken(): Promise<string> {
    try {
      const token = (await Auth.currentSession()).getIdToken().getJwtToken();
      return token;
    } catch (error) {
      if (isString(error)) {
        throw new Error("Unauthorized");
      }
      throw error;
    }
  }

  static async CurrentUserId(): Promise<string> {
    try {
      const user = await Auth.currentAuthenticatedUser();
      return user.attributes.sub;
    } catch (error) {
      throw new Error("Unauthorized");
    }
  }
}
