import { useLocation } from "react-router-dom";

export default function useQueryParams<
  UseQueryExpectedQueryStrings extends Record<string, string>
>(): Partial<UseQueryExpectedQueryStrings> {
  const location = useLocation();
  const params = new URLSearchParams(location.search);
  const queries: Record<string, string> = {};
  // eslint-disable-next-line no-restricted-syntax
  for (const [key, value] of params.entries()) {
    queries[key] = value;
  }
  return (queries as unknown) as Partial<UseQueryExpectedQueryStrings>;
}
